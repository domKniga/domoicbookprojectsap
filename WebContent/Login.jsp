<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="utils.jsp.JspService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="test.css"/>
	<title>Login</title>
</head>
<body>
	<center>
		<fieldset class="head">
			<p>
				<input type="Submit" class="btn btn-danger btn-lg" value="Home" onclick="window.location='index.jsp';">
				<input type="Submit" class="btn btn-danger btn-lg" value="Discussions" onclick="window.location='GetAllDiscussionsServlet';">
				<input type="Submit" class="btn btn-danger btn-lg" value="Obligations" onclick="window.location='CheckObligationServlet';">
				<input type="Submit" class="btn btn-danger btn-lg" value="Messages" onclick="window.location='InboxServlet';">
				<input type="Submit" class="btn btn-danger btn-lg" value="Notifications" onclick="window.location='GetAllNotificationsServlet';">
			</p>
		</fieldset>
	</center>
	
	<center>
		<fieldset class="book">
			<form action="LoginServlet" method="post">
				<h1><b>Domoic book</b></h1>
				
				<h3>
					<label class="field" for="flatNumber">Flat number:</label>
					<input type="text" name="flatNumber" id="flatNumber"/>
				</h3>
				
				<h3>
					<label class="field" for="password">Password:</label>
					<input type="password" name="password" id="password"/>
				</h3>
				
				<h4><input type="Submit" class="btn btn-danger btn-lg" value="Log in"></h4>		
			</form>		
		</fieldset>
	</center>
</body>
</html>