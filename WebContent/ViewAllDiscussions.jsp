<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="utils.jsp.JspService"%>
<%@page import="java.util.*"%>
<%@page import="model.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<%
	    String pageName = JspService.getPageName(request.getRequestURI());
		Status userStatus = Status.ADMIN;
// 		String status = (request.getSession().getAttribute("flatStatus") != null) ? (String) request.getSession().getAttribute("flatStatus") : Status.NORMAL.toString();
		
		out.print(JspService.stylePage());
	%>
	<title>All discussions</title>
	
	<center>
		<fieldset class="head">
			<p>
<!-- 				<input type="Submit" class="btn btn-danger btn-lg" value="Home" onclick="window.location='index.jsp';" > -->
<!-- 				<input type="Submit" class="btn btn-danger btn-lg" value="Discussions" onclick="window.location='GetAllDiscussionsServlet';"> -->
<!-- 				<input type="Submit" class="btn btn-danger btn-lg" value="Obligations" onclick="window.location='CheckObligationServlet';"> -->
<!-- 				<input type="Submit" class="btn btn-danger btn-lg" value="Messages" onclick="window.location='InboxServlet';"> -->
<!-- 				<input type="Submit" class="btn btn-danger btn-lg" value="Notifications" onclick="window.location='GetAllNotificationsServlet';"> -->
				<%
// 					if ("ADMIN".equals(request.getSession().getAttribute("flatStatus"))) {
// 						out.print("<input type='Submit' class='btn btn-danger btn-lg' value='Flats' onclick='window.location=\"AddApartment.jsp\";'>");
// 					}
				
				out.print(JspService.buildHTMLNavigationBar(userStatus));
				%>
<!-- 				<input type="Submit" class="btn btn-danger btn-lg" value="Logout" onclick="window.location='LogoutServlet';"> -->
  		
		</fieldset>
	</center>
	
	<center>
		<fieldset class="book">
			<h1><b>All Discussions</b></h1>	
<!-- 				<input type='radio' name='selected' value='" + discussion.getId() +"' unchecked> -->
			<form name="discussinfo" action="GetDiscussionInfoServlet">
				<table class="tg">
				<%		
					Object[] tableHeaders = { "Id", "Title", "Content", "Date", "Select" };
					String tableHeaderColumns = JspService.buildHTMLTableHeaderColumns(tableHeaders);
					String tableHeaderRow = JspService.buildHTMLTableRow(tableHeaderColumns);
					String tableHead = JspService.buildHTMLTableHead(tableHeaderRow);
					out.print(tableHead);
	
					try{
						@SuppressWarnings("unchecked")
						Vector<Discussion> allDiscussionsList = (Vector<Discussion>)request.getAttribute("allDiscussions");
						for(Discussion discussion : allDiscussionsList){
						    Object[] tableCellValues = 
							{ 
								discussion.getId(),
								discussion.getTitle(),
								discussion.getContent(),
								discussion.getDate(),
								JspService.buildHTMLRadioButton("selected", discussion.getId())
							};
							String tableCells = JspService.buildHTMLTableCells(tableCellValues);
							String tableRow = JspService.buildHTMLTableRow(tableCells);
							String tableBody = JspService.buildHTMLTableBody(tableRow);
							out.print(tableBody);
						}
					}catch(Throwable fault){
					    JspService.logFaultFromJSP(fault, "ViewAllDiscussions.jsp", "allDiscussions");
					}
				%>
				</table>
				
				<h4>
<!-- 					<input type="Submit" class="btn btn-danger btn-lg" value="View comments" onclick="window.location='GetDiscussionInfoServlet';"> -->
					<% 
						out.print(JspService.buildHTMLButton(userStatus,pageName,"viewComments"));
						out.print(JspService.buildHTMLButton(userStatus,pageName,"addDiscussion"));
// 						if ("ADMIN".equals(request.getSession().getAttribute("flatStatus"))) {
// 							out.print("<input type='Submit' class='btn btn-danger btn-lg' value='Add Discussion' onclick='window.location=\"AddDiscussion.jsp\";'>");						
// 						}
					%>
				</h4>
			</form>
		</fieldset>
	</center>
</head>
<body>
</body>
</html>