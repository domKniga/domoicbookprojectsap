<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@page import="utils.jsp.JspService"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="test.css"/>
	<title>Add Flat</title>
</head>
<body>
	<center>
		<fieldset class="head">
  			<center>
				<p> 
					<input type="Submit" class="btn btn-danger btn-lg" value="Home" onclick="window.location='index.jsp';"/>
        			<input type="Submit" class="btn btn-danger btn-lg" value="Discussions" onclick="window.location='GetAllDiscussionsServlet';"/>
        			<input type="Submit" class="btn btn-danger btn-lg" value="Obligations" onclick="window.location='GetAllObligationsServlet';"/>
        			<input type="Submit" class="btn btn-danger btn-lg" value="Messages" onclick="window.location='InboxServlet';"/>
        			<input type="Submit" class="btn btn-danger btn-lg" value="Notifications" onclick="window.location='GetAllNotificationsServlet';"/>
					<input type='Submit' class="btn btn-danger btn-lg" value="Flats" onclick="window.location='AddApartment.jsp';"/>
  					<input type="Submit" class="btn btn-danger btn-lg" value="Logout" onclick="window.location='LogoutServlet';"/>
  				</p>
  			</center>
		</fieldset>
	</center>
	
	<center>
		<fieldset class="book">
			<form action="AddFlatServlet" method="post">
				<center>
					<h1><b>Add Flat</b></h1>
					
					<h3>
						<label class="field" for="flatNumber">Flat Number:</label>
						<input type="text" name="flatNumber" id="flatNumber"/>
					</h3>
					
  					<h3>
  						<label class="field" for="flatSurface">Flat Surface:</label>
  						<input type="text" name="flatSurface" id="flatSurface"/>
  					</h3>
  					
  					<h3>
  						<label class="field" for="Password">Password:</label>
  						<input type="password" name="password"/>
  					</h3>
  					
        			<h3>
        				<label class="field" for="roomCount">Room Count:</label>
        				<input type="text" name="roomCount" id="roomCount"/>
        			</h3>
        			
        			<h3>
        				<label class="field" for="status">Status:</label>
        				<input type="text" name="status" id="status"/>
        			</h3>
        			
  					<h4><input type="Submit" class="btn btn-danger btn-lg" value="Register"></h4>
				</center>
			</form>
		</fieldset>
	</center>
</body>
</html>