package exceptions;

public class NoRecordForEntityException extends Exception {

    /**
     * Original(generated) - object was not yet altered.
     */
    private static final long serialVersionUID = 1912370968327750646L;

    /**
     * @param entityName
     *            - the DB entity class simple name.
     */
    public NoRecordForEntityException(String entityName) {
	super("No record found for DB entity: " + entityName);
    }
}
