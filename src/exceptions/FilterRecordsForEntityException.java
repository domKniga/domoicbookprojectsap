package exceptions;

public class FilterRecordsForEntityException extends Exception {

    /**
     * Original(generated) - object was not yet altered.
     */
    private static final long serialVersionUID = -8754737097683989774L;

    /**
     * @param entityName
     *            - the DB entity class simple name.
     * @param cause
     *            - the causing(underlying) exception.
     */
    public FilterRecordsForEntityException(String entityName, Throwable cause) {
	super("Failed filtering of DB entity: " + entityName, cause);
    }

}
