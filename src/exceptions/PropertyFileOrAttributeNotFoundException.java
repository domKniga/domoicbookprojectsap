package exceptions;

public class PropertyFileOrAttributeNotFoundException extends Exception {

    /**
     * Original(generated) - object was not yet altered.
     */
    private static final long serialVersionUID = -8143757945183249257L;

    /**
     * @param missingItem
     *            - the missing item.
     */
    public PropertyFileOrAttributeNotFoundException(String missingItem) {
	super("Property file or property named (" + missingItem + ") was not found!");
    }

}
