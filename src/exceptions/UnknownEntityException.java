package exceptions;

public class UnknownEntityException extends Exception {

    /**
     * Original(generated) - object was not yet altered.
     */
    private static final long serialVersionUID = 1493842885630335335L;

    /**
     * @param entityName
     *            - the DB entity class simple name.
     * @param cause
     *            - the causing(underlying) exception.
     */
    public UnknownEntityException(String entityName, Throwable cause) {
	super("Attempted creation of unknown DB entity: " + entityName, cause);
    }

}
