package model;

/**
 * Enumeration of the living situation status of a resident. NORMAL - any
 * resident. ADMIN - housekeeper.
 * 
 * @authors Georgi Iliev, Vencislav Penev
 */
public enum Status {
    NORMAL, ADMIN
}
