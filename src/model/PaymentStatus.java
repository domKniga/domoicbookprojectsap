package model;

/**
 * Enumeration for the payment status.
 * 
 * @authors Georgi Iliev, Vencislav Penev
 */
public enum PaymentStatus {
    NOT_PAID, PAID
}
