package utils.jsp;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.mysql.jdbc.StringUtils;

import model.Status;

/**
 * Utility methods designed to simplify expressions and minimize Java code
 * fragments in the actual JSP pages.
 * 
 * @author Georgi Iliev
 */
public class JspService {

    private static final Logger LOGGER = Logger.getLogger(JspService.class);

    private static final String COMMON_PAGE_STYLE_PROPERTIES = "commonPageStyleAttributes.properties";
    private static final String COMMON_BUTTON_PROPERTIES = "commonButtonAttributes.properties";
    private static final String COMMON_RADIO_BUTTON_PROPERTIES = "commonRadioButtonAttributes.properties";
    private static final String COMMON_TABLE_HEADER_PROPERTIES = "commonTableHeaderAttributes.properties";
    private static final String COMMON_TABLE_CELL_PROPERTIES = "commonTableCellAttributes.properties";

    // TODO: Page specific context decide where.
    // private static final String DATA_ATTRIBUTE_ALL_DISCUSSIONS =
    // "allDiscussions";

    private JspService() {
    }

    /**
     * Logs faults(exceptions AND errors) as a last resort when passing through
     * a JSP.
     * 
     * @param fault
     *            - the fault(exception OR error).
     * @param causingJSP
     *            - the JSP name where the fault originated from.
     * @param affectedAttribute
     *            - the affected session/request attribute if applicable.
     */
    public static void logFaultFromJSP(Throwable fault, String causingJSP, String affectedAttribute) {
	String errorMessage = "Fault in: " + causingJSP + "\n" + "Concerns attribute: " + affectedAttribute + "\n";
	LOGGER.warn(errorMessage, fault);
    }

    public static String buildHTMLTableHead(Object value) {
	return new HtmlTag(HtmlSyntax.TABLE_HEAD, value, null).buildTag();
    }

    public static String buildHTMLTableBody(Object value) {
	return new HtmlTag(HtmlSyntax.TABLE_BODY, value, null).buildTag();
    }

    public static String buildHTMLTableRow(Object value) {
	return new HtmlTag(HtmlSyntax.TABLE_ROW, value, null).buildTag();
    }

    public static String buildHTMLTableCells(Object... values) {
	List<HtmlAttribute> attributes = new ArrayList<HtmlAttribute>();

	Properties propertyFile = getPropertyFile(COMMON_TABLE_CELL_PROPERTIES);

	attributes.add(buildHtmlAttributeFromPropertyFile("class", propertyFile));
	clearEmptyOrUnknownValueAttributes(attributes);

	StringBuilder htmlTableCellsBuilder = new StringBuilder();

	for (Object value : values) {
	    htmlTableCellsBuilder.append(new HtmlTag(HtmlSyntax.TABLE_CELL, value, attributes).buildTag());
	}

	return htmlTableCellsBuilder.toString();
    }

    public static String buildHTMLTableHeaderColumns(Object... values) {
	List<HtmlAttribute> attributes = new ArrayList<HtmlAttribute>();

	Properties propertyFile = getPropertyFile(COMMON_TABLE_HEADER_PROPERTIES);

	attributes.add(buildHtmlAttributeFromPropertyFile("class", propertyFile));
	clearEmptyOrUnknownValueAttributes(attributes);

	StringBuilder htmlHeaderColumnsBuilder = new StringBuilder();

	for (Object value : values) {
	    htmlHeaderColumnsBuilder.append(new HtmlTag(HtmlSyntax.TABLE_HEADER_COLUMN, value, attributes).buildTag());
	}

	return htmlHeaderColumnsBuilder.toString();
    }

    public static String buildHTMLRadioButton(String name, Object value) {
	List<HtmlAttribute> attributes = new ArrayList<HtmlAttribute>();

	Properties propertyFile = getPropertyFile(COMMON_RADIO_BUTTON_PROPERTIES);

	attributes.add(buildHtmlAttributeFromPropertyFile("type", propertyFile));
	attributes.add(new HtmlAttribute("name", name));
	attributes.add(new HtmlAttribute("value", String.valueOf(value)));
	attributes.add(buildHtmlAttributeFromPropertyFile("checked", propertyFile));
	clearEmptyOrUnknownValueAttributes(attributes);

	return new HtmlTag(HtmlSyntax.INPUT, null, attributes).buildTag();
    }

    public static String buildHTMLNavigationBar(Status userStatus) {
	StringBuilder navigationBarBuilder = new StringBuilder();

	navigationBarBuilder // nl
		.append(buildHTMLButton(userStatus, COMMON_BUTTON_PROPERTIES, "home"))
		.append(buildHTMLButton(userStatus, COMMON_BUTTON_PROPERTIES, "messages"))
		.append(buildHTMLButton(userStatus, COMMON_BUTTON_PROPERTIES, "discussions"))
		.append(buildHTMLButton(userStatus, COMMON_BUTTON_PROPERTIES, "notifications"))
		.append(buildHTMLButton(userStatus, COMMON_BUTTON_PROPERTIES, "obligations"))
		.append(buildHTMLButton(userStatus, COMMON_BUTTON_PROPERTIES, "flats"))
		.append(buildHTMLButton(userStatus, COMMON_BUTTON_PROPERTIES, "logout"));

	return navigationBarBuilder.toString();
    }

    public static String buildHTMLButton(Status userStatus, String pageOrPropertyFileName, String buttonNameTag) {
	if (StringUtils.isNullOrEmpty(pageOrPropertyFileName) || StringUtils.isNullOrEmpty(buttonNameTag)) {
	    // TODO: throw exc or handle quietly.
	}

	// System.out.println(pageName);
	String propertyFileName = (pageOrPropertyFileName.endsWith(".properties")) ? pageOrPropertyFileName
		: pageOrPropertyFileName.substring(0, pageOrPropertyFileName.lastIndexOf('.')).concat(".properties");
	// System.out.println(propertyFileName);

	Properties propertyFile = getPropertyFile(propertyFileName);
	if (propertyFile.isEmpty()) {
	    // TODO: throw exc or handle quietly.
	}

	String value = getAttributeValueFromPropertyFile(buttonNameTag + "BtnValue", propertyFile);
	String link = getAttributeValueFromPropertyFile(buttonNameTag + "BtnLink", propertyFile);
	String restricted = getAttributeValueFromPropertyFile(buttonNameTag + "BtnRestricted", propertyFile);

	return buildHTMLButton(userStatus, value, link, restricted);
    }

    private static String buildHTMLButton(Status userStatus, String value, String link, String restricted) {
	if (Boolean.valueOf(restricted) && !Status.ADMIN.equals(userStatus)) {
	    // TODO: throw exc or handle quietly.
	    return "";
	}

	List<HtmlAttribute> attributes = new ArrayList<HtmlAttribute>();

	Properties commonButtonPropertyFile = getPropertyFile(COMMON_BUTTON_PROPERTIES);

	attributes.add(buildHtmlAttributeFromPropertyFile("type", commonButtonPropertyFile));
	attributes.add(buildHtmlAttributeFromPropertyFile("class", commonButtonPropertyFile));
	attributes.add(new HtmlAttribute("value", value));
	attributes.add(new HtmlAttribute("onClick", "window.location='" + link + "'"));
	clearEmptyOrUnknownValueAttributes(attributes);

	return new HtmlTag(HtmlSyntax.INPUT, null, attributes).buildTag();
    }

    public static String stylePage() {
	List<HtmlAttribute> attributes = new ArrayList<HtmlAttribute>();

	Properties propertyFile = getPropertyFile(COMMON_PAGE_STYLE_PROPERTIES);

	attributes.add(buildHtmlAttributeFromPropertyFile("rel", propertyFile));
	attributes.add(buildHtmlAttributeFromPropertyFile("type", propertyFile));
	HtmlAttribute altAttribute = buildHtmlAttributeFromPropertyFile("href", propertyFile);

	// attributes.add(buildHtmlAttributeFromPropertyFile("KOrrr",
	// propertyFile));
	// attributes.add(buildHtmlAttributeFromPropertyFile("qjj",
	// propertyFile));

	clearEmptyOrUnknownValueAttributes(attributes);

	return buildHtmlTagsWithAlternativeValues(HtmlSyntax.EXTERNAL_LINK, attributes, altAttribute);
    }

    private static String buildHtmlTagsWithAlternativeValues(String syntaxKeyword, List<HtmlAttribute> commonAttributes,
	    HtmlAttribute attributeWithAlternativeValues) {
	List<HtmlAttribute> fullAttributeList;
	StringBuilder alternativeTagsBuilder = new StringBuilder();

	String[] alternativeValues = attributeWithAlternativeValues.getAttributeValue().split(",");
	for (String alternative : alternativeValues) {
	    fullAttributeList = new ArrayList<HtmlAttribute>(commonAttributes);
	    fullAttributeList.add(new HtmlAttribute(attributeWithAlternativeValues.getAttributeName(), alternative));
	    clearEmptyOrUnknownValueAttributes(fullAttributeList);

	    alternativeTagsBuilder.append(new HtmlTag(syntaxKeyword, null, fullAttributeList).buildTag());
	}

	return alternativeTagsBuilder.toString();
    }

    private static void clearEmptyOrUnknownValueAttributes(List<HtmlAttribute> attributes) {
	ListIterator<HtmlAttribute> attributesListIterator = attributes.listIterator();

	while (attributesListIterator.hasNext()) {
	    HtmlAttribute attribute = (HtmlAttribute) attributesListIterator.next();

	    if (HtmlAttribute.VALUE_UNKNOWN.equals(attribute.getAttributeValue())) {
		String warningMessage = "Removing attribute[" + attribute.getAttributeName() + "] - value is unknown!";
		LOGGER.warn(warningMessage);

		System.out.println(warningMessage);

		attributesListIterator.remove();
	    }
	}
    }

    private static HtmlAttribute buildHtmlAttributeFromPropertyFile(String attributeName, Properties propertyFile) {
	String attributeValue = getAttributeValueFromPropertyFile(attributeName, propertyFile);

	return new HtmlAttribute(attributeName, attributeValue);
    }

    private static String getAttributeValueFromPropertyFile(String valueAttributeName, Properties propertyFile) {
	// TODO: Handle quietly or boom.
	if (propertyFile.isEmpty()) {
	    return HtmlAttribute.VALUE_UNKNOWN;
	}

	String valueAsString = String.valueOf(propertyFile.get(valueAttributeName));

	return (valueAsString != null) ? valueAsString : HtmlAttribute.VALUE_UNKNOWN;
    }

    private static Properties getPropertyFile(String propertyFileName) {
	Properties attributesPropertyFile = new Properties();

	InputStream attributeAsStream = JspService.class.getResourceAsStream(propertyFileName);
	if (attributeAsStream == null) {
	    LOGGER.warn("Property file not found!");
	    return new Properties();
	}

	try {
	    attributesPropertyFile.load(attributeAsStream);

	    return attributesPropertyFile;
	} catch (IOException | IllegalArgumentException e) {
	    LOGGER.warn(e.getMessage(), e);
	    return new Properties();
	}
    }

    public static String getPageName(String requestUri) {
	return requestUri.substring(requestUri.lastIndexOf("/") + 1);
    }

    public static void main(String[] args) {
	Status userStatus = Status.ADMIN;

	System.out.println(stylePage());
	System.out.println();

	// Nav. bar
	System.out.println(buildHTMLNavigationBar(userStatus));
	System.out.println();

	// Table head structure
	String headers = buildHTMLTableHeaderColumns("AA", "AB");
	String headerColumns = buildHTMLTableHeaderColumns(headers);
	String headerRow = buildHTMLTableRow(headerColumns);
	System.out.println(buildHTMLTableHead(headerRow));
	System.out.println();

	// Table cell structure
	String tableCells = buildHTMLTableCells("A", 3, buildHTMLRadioButton("selected", 3));
	String tableRow = buildHTMLTableRow(tableCells);
	System.out.println(buildHTMLTableBody(tableRow));
	System.out.println();

	// Form buttons
	System.out.print(JspService.buildHTMLButton(userStatus, "ViewAllDiscussions.jsp", "viewComments"));
	System.out.print(JspService.buildHTMLButton(userStatus, "ViewAllDiscussions.jsp", "addDiscussion"));
    }

}
