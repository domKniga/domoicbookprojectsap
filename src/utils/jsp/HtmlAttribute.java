package utils.jsp;

import java.util.Properties;

class HtmlAttribute {
    private String attributeName;
    private String attributeValue;
    
    static final String VALUE_UNKNOWN = "unknown";

    HtmlAttribute(String attributeName) {
	this.attributeName = attributeName;
	this.attributeValue = VALUE_UNKNOWN;
    }

    HtmlAttribute(String attributeName, Object attributeValue) {
	this.attributeName = attributeName;
	this.attributeValue = String.valueOf(attributeValue);
    }

    // HtmlAttribute(String attributeName, Properties attributesPropertyFile)
    // throws IOException, PropertyFileOrAttributeNotFoundException {
    //
    // String attributeValue =
    // attributesPropertyFile.getProperty(attributeName);
    // if (attributeValue == null) {
    // throw new PropertyFileOrAttributeNotFoundException(attributeName);
    // }
    //
    // this.attributeName = attributeName;
    // this.attributeValue = attributeValue;
    // }

    HtmlAttribute(String attributeName, Properties attributesPropertyFile) {
	String attributeValue = attributesPropertyFile.getProperty(attributeName);

	if (attributeValue == null) {
	    attributeValue = VALUE_UNKNOWN;
	}

	this.attributeName = attributeName;
	this.attributeValue = attributeValue;
    }

    public String getAttributeName() {
	return this.attributeName;
    }

    public void setAttributeName(String attributeName) {
	this.attributeName = attributeName;
    }

    public String getAttributeValue() {
	return this.attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
	this.attributeValue = attributeValue;
    }
}