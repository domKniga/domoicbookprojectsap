package utils.jsp;

/**
 * Syntax keyword constants for HTML elements prior to HTML5 standard.
 * 
 * @author Georgi Iliev
 */
class HtmlSyntax {
    static final String EXTERNAL_LINK = "link";
    static final String INPUT = "input";
    static final String TABLE = "table";
    static final String TABLE_HEAD = "thead";
    static final String TABLE_BODY = "tbody";
    static final String TABLE_ROW = "tr";
    static final String TABLE_CELL = "td";
    static final String TABLE_HEADER_COLUMN = "th";

    private HtmlSyntax() {
    }

}
