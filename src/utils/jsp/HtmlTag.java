package utils.jsp;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Representation of a HTML tag prior to the HTML5 standard.
 * 
 * @author Georgi Iliev
 */
class HtmlTag {
    private String tagName;
    private Object value;
    private String openingSequence;
    private String closingSequence;

    private List<HtmlAttribute> attributes;
    
    HtmlTag(String tagName, Object value, List<HtmlAttribute> attributes) {
	this.tagName = tagName;
	this.value = (value != null) ? value : "";
	this.attributes = (attributes != null) ? attributes : Collections.emptyList();

	openingSequence = "<" + this.tagName;
	closingSequence = "</" + this.tagName + ">";
    }

    String buildTag() {
	StringBuilder tagBuilder = new StringBuilder();

	tagBuilder.append(openingSequence);

	// Append attributes if applicable
	for (HtmlAttribute attribute : attributes) {
	    tagBuilder // nl
		    .append(" ") // nl
		    .append(attribute.getAttributeName()) // nl
		    .append("=") // nl
		    .append(StringUtils.wrap(attribute.getAttributeValue(), '"'));
	}
	tagBuilder // nl
		.append("".equals(value) ? "/" : "") // nl
		.append(">") // nl
		.append(value) // nl
		.append("".equals(value) ? "" : closingSequence) // nl
		.append("\n");

	return tagBuilder.toString();
    }

}
