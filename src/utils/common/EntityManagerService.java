package utils.common;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import exceptions.NoRecordForEntityException;
import exceptions.UnknownEntityException;

/**
 * Entity manager service - provides some wrapped EntityManager methods for
 * improved error/exception handling.
 * 
 * @authors Georgi Iliev, Vencislav Penev
 */
public class EntityManagerService {

    private static final Logger LOGGER = Logger.getLogger(EntityManagerService.class);

    private static final String PERSISTENCE_UNIT = "domoic-persistence-unit";

    public static final EntityManager ENTITY_MANAGER = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT)
	    .createEntityManager();

    private EntityManagerService() {
    }

    /**
     * Wrapper method to ensure the search by PK is handled in case of a null or
     * unchecked exception.
     * 
     * @param entityClass
     *            - the class of the DB entity.
     * @param primaryKey
     *            - the primary key of the DB entity.
     * @return The found object, representing the DB record, if such is present.
     * @throws UnknownEntityException
     *             If the DB entity is not recognized as a valid one.
     * @throws NoRecordForEntityException
     *             If there is no record returned by the search.
     */
    public static <T> Object find(Class<T> entityClass, Object primaryKey)
	    throws UnknownEntityException, NoRecordForEntityException {
	try {
	    Object foundRecord = ENTITY_MANAGER.find(entityClass, primaryKey);
	    if (foundRecord == null) {
		LOGGER.error("No record for Entity <" + entityClass.getSimpleName() + "> ; PK <" + primaryKey + "> )");
		throw new NoRecordForEntityException(entityClass.getSimpleName());
	    }

	    return foundRecord;
	} catch (IllegalArgumentException iae) {
	    LOGGER.error("ERROR: " + iae.getMessage());
	    throw new UnknownEntityException(entityClass.getSimpleName(), iae);
	}
    }

    // /**
    // * HOW TO CREATE AN ENTITY MANAGER TRADITIONALLY.
    // *
    // * @return
    // */
    // public static EntityManager getEntityManager() {
    // return
    // Persistence.createEntityManagerFactory(PERSISTENCE_UNIT).createEntityManager();
    // }

    /**
     * Wrapper method to ensure runtime exception scenario is handled not
     * propagated directly.
     * 
     * @param filterQuery
     *            - the filter query string.
     * @param resultClass
     *            - the class that the result is expected to be assignable from.
     * @return
     * @throws UnknownEntityException
     *             If the filtered result objects are not assignable from the
     *             given parameter class.
     */
    public static <T> TypedQuery<T> createQuery(String filterQuery, Class<T> resultClass)
	    throws UnknownEntityException {
	try {
	    return ENTITY_MANAGER.createQuery(filterQuery, resultClass);
	} catch (IllegalArgumentException iae) {
	    LOGGER.error("ERROR: " + iae.getMessage());
	    throw new UnknownEntityException(resultClass.getSimpleName(), iae);
	}
    }

    // TODO: Wrap delete method also.
}