package utils.common;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;

/**
 * Generic utility methods for basic validation.
 * 
 * @author Georgi Iliev
 */
public class ValidationService {

    private ValidationService() {
    }

    /**
     * Utility method to ensure no null is present in a sequence of
     * attributes(parameters).
     * 
     * @param attributes
     *            - the data attributes to be validated.
     * @return TRUE in case null is found, FALSE otherwise.
     */
    public static boolean foundNullAttribute(Object... attributes) {
	return Arrays.asList(attributes).contains(null);
    }

    // TODO: Might become redundant if ExceptionUtils methods are sufficient
    /**
     * Gets the error/exception stack trace down to the root as a string.
     * 
     * @param throwable
     *            - the top level error/exception.
     * @return A string representation of the stack trace.
     */
    public static String getStackTraceAsString(Throwable throwable) {
	StringWriter stackTraceElements = new StringWriter();
	throwable.printStackTrace(new PrintWriter(stackTraceElements));

	return stackTraceElements.toString();
    }

}
