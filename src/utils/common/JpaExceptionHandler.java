package utils.common;

import org.apache.log4j.Logger;
import org.eclipse.persistence.exceptions.ExceptionHandler;

public class JpaExceptionHandler implements ExceptionHandler {

    private static final Logger LOGGER = Logger.getLogger(JpaExceptionHandler.class);

    @Override
    public Object handleException(RuntimeException exception) {
	String stackTraceToRoot = ValidationService.getStackTraceAsString(exception);

	// TODO: Seems very bad...
	System.err.println("IN " + this.getClass().getSimpleName());
	System.err.println(stackTraceToRoot);

	LOGGER.warn(stackTraceToRoot, exception);
	return new Object();
    }

}
