package web;

import static utils.common.EntityManagerService.ENTITY_MANAGER;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.PrivateMessageDAO;
import model.Flat;
import model.PrivateMessage;
import utils.common.ValidationService;

public class AddMessageServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = 890180849454297775L;

    private static final Logger LOGGER = Logger.getLogger(AddMessageServlet.class);

    public AddMessageServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    response.sendRedirect("OutboxServlet");
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	String title = String.valueOf(request.getParameter("title"));
	String content = String.valueOf(request.getParameter("content"));
	Integer senderNumber = ((Flat) request.getSession().getAttribute("currentFlat")).getNumber();
	Integer receiverNumber = Integer.valueOf(request.getParameter("receiverNumber"));

	if (ValidationService.foundNullAttribute(title, content, senderNumber, receiverNumber)) {
	    String errorReport = "No(null) value for a message related attribute found:" // nl
		    + "\n Title: " + title // nl
		    + "\n Content: " + content // nl
		    + "\n Sender: " + senderNumber // nl
		    + "\n Recipient: " + receiverNumber;
	    LOGGER.error(errorReport);
	    throw new ServletException(errorReport);
	}

	PrivateMessage pm = new PrivateMessage();
	pm.setTitle(title);
	pm.setContent(content);

	new PrivateMessageDAO(ENTITY_MANAGER).addPrivateMessage(pm, senderNumber, receiverNumber);

	doGet(request, response);
    }

}
