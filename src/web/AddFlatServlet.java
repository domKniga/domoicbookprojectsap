package web;

import static utils.common.EntityManagerService.ENTITY_MANAGER;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.FlatDAO;
import model.Flat;
import model.Status;
import utils.common.ValidationService;

public class AddFlatServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = 1666483164711399221L;

    private static final Logger LOGGER = Logger.getLogger(AddFlatServlet.class);

    public AddFlatServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    response.sendRedirect("index.jsp");
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	Integer flatNumber = Integer.valueOf(request.getParameter("flatNumber"));
	Integer roomCount = Integer.valueOf(request.getParameter("roomCount"));
	Double surface = Double.valueOf(request.getParameter("flatSurface"));
	String password = String.valueOf(request.getParameter("password"));
	String status = String.valueOf(request.getParameter("status"));

	if (ValidationService.foundNullAttribute(flatNumber, roomCount, surface, password, status)) {
	    String errorReport = "No(null) value for a flat related attribute found:" // nl
		    + "\n Flat: " + flatNumber // nl
		    + "\n Room count: " + roomCount // nl
		    + "\n Surface: " + surface // nl
		    + "\n Password: " + password // nl
		    + "\n Status: " + status;
	    LOGGER.error(errorReport);
	    throw new ServletException(errorReport);
	}

	Flat flat = new Flat();
	flat.setNumber(flatNumber);
	flat.setRoomCount(roomCount);
	flat.setFlatSurface(surface);
	flat.setPassword(password);
	flat.setStatus(status.equals(Status.NORMAL.toString()) ? Status.NORMAL : Status.ADMIN);

	new FlatDAO(ENTITY_MANAGER).addFlat(flat);

	doGet(request, response);
    }

}