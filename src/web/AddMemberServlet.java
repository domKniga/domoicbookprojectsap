package web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.ResidentDAO;
import model.Resident;
import utils.common.EntityManagerService;
import utils.common.ValidationService;

public class AddMemberServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = 3944612887137086824L;

    private static final Logger LOGGER = Logger.getLogger(AddMemberServlet.class);

    public AddMemberServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	// try {
	// response.sendRedirect("index.jsp");
	// } catch (IOException ie) {
	// LOGGER.error(ie.getMessage());
	// throw new ServletException(ie.getMessage(), ie);
	// }
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	Integer flatNumber = Integer.valueOf(request.getParameter("flatNumber"));
	String firstName = String.valueOf(request.getParameter("firstName"));
	String lastName = String.valueOf(request.getParameter("lastName"));

	if (ValidationService.foundNullAttribute(flatNumber, firstName, lastName)) {
	    String errorReport = "No(null) value for a resident related attribute found:" // nl
		    + "\n Flat: " + flatNumber // nl
		    + "\n First name: " + firstName // nl
		    + "\n Last name: " + lastName;
	    LOGGER.error(errorReport);
	    throw new ServletException(errorReport);
	}

	Resident resident = new Resident();
	resident.setFirstName(firstName);
	resident.setLastName(lastName);

	new ResidentDAO(EntityManagerService.ENTITY_MANAGER).addResident(resident, flatNumber);

	doGet(request, response);
    }

}
