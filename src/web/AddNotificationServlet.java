package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.NotificationDAO;
import model.Flat;
import model.Notification;
import utils.common.EntityManagerService;
import utils.common.ValidationService;

public class AddNotificationServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = -6612484866489524199L;

    private static final Logger LOGGER = Logger.getLogger(AddNotificationServlet.class);

    public AddNotificationServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    response.sendRedirect("GetAllNotificationsServlet");
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	Integer flatNumber = ((Flat) request.getSession().getAttribute("currentFlat")).getNumber();
	String title = String.valueOf(request.getParameter("title"));
	String content = String.valueOf(request.getParameter("content"));

	if (ValidationService.foundNullAttribute(flatNumber, title, content)) {
	    String errorReport = "No(null) value for a notification related attribute found:" // nl
		    + "\n Flat: " + flatNumber // nl
		    + "\n Title: " + title // nl
		    + "\n Content: " + content;
	    LOGGER.error(errorReport);
	    throw new ServletException(errorReport);
	}

	Notification note = new Notification();
	note.setTitle(title);
	note.setContent(content);

	new NotificationDAO(EntityManagerService.ENTITY_MANAGER).addNotification(note, flatNumber);

	doGet(request, response);
    }

}
