package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;

/**
 * Servlet to handle all exception/error cases in a more user-friendly way.
 * 
 * @author Georgi Iliev
 */
public class ErrorHandlingServlet extends HttpServlet {

    /**
     * Original(generated) - object was not yet altered.
     */
    private static final long serialVersionUID = 2129200327922500907L;

    private static final Logger LOGGER = Logger.getLogger(ErrorHandlingServlet.class);

    public ErrorHandlingServlet() {
	super();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
	processError(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
	processError(request, response);
    }

    private void processError(HttpServletRequest request, HttpServletResponse response) throws IOException {
	Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
	Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");

	String servletName = (String) request.getAttribute("javax.servlet.error.servlet_name");
	servletName = (servletName == null) ? "Unknown" : servletName;

	String requestUri = (String) request.getAttribute("javax.servlet.error.request_uri");
	requestUri = (requestUri == null) ? "Unknown" : requestUri;

	// TODO: Test/compare BOTH options - may be use apache.
	// String rootCauseTrail =
	// ValidationService.getStackTraceAsString(throwable);
	// LOGGER.error("\n Exception/Error: " + throwable // nl
	// + "\n Status code: " + statusCode // nl
	// + "\n Servlet: " + servletName // nl
	// + "\n Request URI: " + requestUri // nl
	// + "\n" + StringUtils.join(rootCauseTrail, "\n"));

	String[] rootCauseTrail = ExceptionUtils.getRootCauseStackTrace(throwable);

	LOGGER.error("\n Exception/Error: " + throwable // nl
		+ "\n Status code: " + statusCode // nl
		+ "\n Servlet: " + servletName // nl
		+ "\n Request URI: " + requestUri // nl
		+ "\n" + StringUtils.join(rootCauseTrail, "\n"));

	response.setContentType("text/html");

	// TODO: Refactor/redesign/beautify the error page.

	PrintWriter out = response.getWriter();
	out.write("<html><head><title>Exception/Error Details</title></head><body>");
	if (statusCode != 500) {
	    out.write("<h3>Error Details</h3>");
	    out.write("<strong>Status Code</strong>:" + statusCode + "<br>");
	    out.write("<strong>Requested URI</strong>:" + requestUri);
	} else {
	    out.write("<h3>Exception Details</h3>");
	    out.write("<ul><li>Servlet Name:" + servletName + "</li>");
	    out.write("<li>Exception Name:" + throwable.getClass().getName() + "</li>");
	    out.write("<li>Requested URI:" + requestUri + "</li>");
	    out.write("<li>Exception Message:" + throwable.getMessage() + "</li>");
	    out.write("</ul>");
	}

	out.write("<br><br>");
	out.write("<a href=\"index.jsp\">Home Page</a>");
	out.write("</body></html>");
    }

}
