package web;

import static utils.common.EntityManagerService.ENTITY_MANAGER;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.PaymentDAO;
import utils.common.ValidationService;

public class UpdatePaymentServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = 6567699397200670667L;

    private static final Logger LOGGER = Logger.getLogger(UpdatePaymentServlet.class);

    public UpdatePaymentServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    response.sendRedirect("GetAllObligationsServlet");
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	String[] selectedObligations = request.getParameterValues("selected");
	if (selectedObligations == null) {
	    String errorMessage = "No obligation/s selected!";
	    LOGGER.error(errorMessage);
	    throw new ServletException(errorMessage);
	}

	for (int i = 0; i < selectedObligations.length; i++) {
	    String[] obligationAttributes = selectedObligations[i].split("[-]");
	    Integer flatNumber = Integer.valueOf(obligationAttributes[1]);
	    Integer obligationId = Integer.valueOf(obligationAttributes[0]);

	    if (ValidationService.foundNullAttribute(flatNumber, obligationId)) {
		String errorReport = "No(null) value for a payment related attribute found:" // nl
			+ "\n Flat: " + flatNumber // nl
			+ "\n Obligation: " + obligationId;
		LOGGER.error(errorReport);
		throw new ServletException(errorReport);
	    }

	    new PaymentDAO(ENTITY_MANAGER).updatePayment(flatNumber, obligationId);
	}

	doGet(request, response);
    }
}
