package web;

import static utils.common.EntityManagerService.ENTITY_MANAGER;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.PrivateMessageDAO;
import model.Flat;
import model.PrivateMessage;

public class OutboxServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = -603547576260778397L;

    private static final Logger LOGGER = Logger.getLogger(OutboxServlet.class);

    public OutboxServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	Integer flatNumber = ((Flat) request.getSession().getAttribute("currentFlat")).getNumber();

	Vector<PrivateMessage> outbox = new PrivateMessageDAO(ENTITY_MANAGER).getInfoAboutOutbox(flatNumber);
	request.setAttribute("outboxMessages", outbox);

	try {
	    request.getRequestDispatcher("ViewOutbox.jsp").forward(request, response);
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    }

}
