package web;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import dao.FlatDAO;
import exceptions.NoRecordForEntityException;
import exceptions.UnknownEntityException;
import model.Flat;
import utils.common.EntityManagerService;

public class LoginServlet extends HttpServlet implements Serializable {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = 4486753878099856253L;

    private static final Logger LOGGER = Logger.getLogger(LoginServlet.class);

    public LoginServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    request.getRequestDispatcher("Login.jsp").forward(request, response);
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    String flatAttemptingLogin = request.getParameter("flatNumber");
	    String passwordOfFlatAttemptingLogin = request.getParameter("password");
	    if ((flatAttemptingLogin == null || !StringUtils.isNumeric(flatAttemptingLogin))
		    || passwordOfFlatAttemptingLogin.equals("")) {
		response.sendRedirect("Login.jsp");
		return;
	    } else {
		Integer flatNumber = Integer.valueOf(flatAttemptingLogin);
		String password = String.valueOf(passwordOfFlatAttemptingLogin);

		boolean isValidFlat = new FlatDAO(EntityManagerService.ENTITY_MANAGER).validateFlat(flatNumber,
			password);

		// TODO: Do you need this at all?
		// response.setContentType("text/html");

		try {
		    if (isValidFlat) {
			Flat flat = (Flat) EntityManagerService.find(Flat.class, flatNumber);
			request.getSession().setAttribute("currentFlat", flatNumber);
			request.getSession().setAttribute("flatStatus", flat.getStatus().toString());

			request.getRequestDispatcher("index.jsp").forward(request, response);
			return;
		    } else {
			doGet(request, response);
		    }
		} catch (UnknownEntityException | NoRecordForEntityException e) {
		    LOGGER.error(e.getMessage());
		    throw new ServletException(e.getMessage(), e);
		}
	    }
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

}
