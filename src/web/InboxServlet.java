package web;

import static utils.common.EntityManagerService.ENTITY_MANAGER;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.PrivateMessageDAO;
import model.Flat;
import model.PrivateMessage;

public class InboxServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = 8090863243063571135L;

    private static final Logger LOGGER = Logger.getLogger(InboxServlet.class);

    public InboxServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	Object currentlyLoggedInFlat = request.getSession().getAttribute("currentFlat");
	Object currentlyLoggedInFlatStatus = request.getSession().getAttribute("flatStatus");

	try {
	    if (currentlyLoggedInFlat == null || currentlyLoggedInFlatStatus == null) {
		response.sendRedirect("Login.jsp");
		return;
	    }

	    Integer flatNumber = ((Flat) request.getSession().getAttribute("currentFlat")).getNumber();

	    Vector<PrivateMessage> inbox = new PrivateMessageDAO(ENTITY_MANAGER).getInfoAboutInbox(flatNumber);
	    request.setAttribute("inboxMessages", inbox);

	    request.getRequestDispatcher("Inbox.jsp").forward(request, response);
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    }

}
