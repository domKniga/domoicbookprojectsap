package web;

import static utils.common.EntityManagerService.ENTITY_MANAGER;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.AnswerDAO;
import model.Answer;

public class GetDiscussionInfoServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = 2399162864287427251L;

    private static final Logger LOGGER = Logger.getLogger(GetDiscussionInfoServlet.class);

    public GetDiscussionInfoServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	if (request.getParameter("selected") == null) {
	    String errorReport = "No discussion was selected!";
	    LOGGER.error(errorReport);
	    throw new ServletException(errorReport);
	}

	Integer id = Integer.valueOf(request.getParameter("selected"));

	Vector<Answer> answers = new AnswerDAO(ENTITY_MANAGER).getInfoAboutDiscuss(id);
	request.setAttribute("discussionInfo", answers);
	request.getSession().setAttribute("chosen", id);

	try {
	    request.getRequestDispatcher("ViewDiscussion.jsp").forward(request, response);
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    }

}
