package web;

import static utils.common.EntityManagerService.ENTITY_MANAGER;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.PaymentDAO;
import model.Flat;
import model.Payment;

public class GetAllObligationsServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = -8963454152531510877L;

    private static final Logger LOGGER = Logger.getLogger(GetAllObligationsServlet.class);

    public GetAllObligationsServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    Object currentlyLoggedInFlat = request.getSession().getAttribute("currentFlat");
	    Object currentlyLoggedInFlatStatus = request.getSession().getAttribute("flatStatus");
	    if (currentlyLoggedInFlat == null || currentlyLoggedInFlatStatus == null) {
		response.sendRedirect("Login.jsp");
		return;
	    }

	    Integer flatNumber = ((Flat) currentlyLoggedInFlat).getNumber();

	    Vector<Payment> obligations = new PaymentDAO(ENTITY_MANAGER).getPaymentsByNumber(flatNumber);
	    request.setAttribute("allObligations", obligations);

	    request.getRequestDispatcher("ViewAllObligations.jsp").forward(request, response);
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    }

}
