package web;

import static utils.common.EntityManagerService.ENTITY_MANAGER;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.ObligationDAO;
import model.Obligation;
import utils.common.ValidationService;

public class AddObligationServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = -533942245633878560L;

    private static final Logger LOGGER = Logger.getLogger(AddObligationServlet.class);

    public AddObligationServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    response.sendRedirect("GetAllObligationsServlet");
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	String description = String.valueOf(request.getParameter("description"));
	String deadline = String.valueOf(request.getParameter("deadline"));
	Double debt = Double.valueOf(request.getParameter("debt"));

	if (ValidationService.foundNullAttribute(description, deadline, debt)) {
	    String errorReport = "No(null) value for an obligation related attribute found:" // nl
		    + "\n Description: " + description // nl
		    + "\n Deadline: " + deadline // nl
		    + "\n Debt: " + debt;
	    LOGGER.error(errorReport);
	    throw new ServletException(errorReport);
	}

	Obligation obligation = new Obligation();
	obligation.setDescription(description);
	obligation.setDeadline(deadline);
	obligation.setDebt(debt);

	new ObligationDAO(ENTITY_MANAGER).addObligation(obligation);

	doGet(request, response);
    }

}
