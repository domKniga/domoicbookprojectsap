package web;

import static utils.common.EntityManagerService.ENTITY_MANAGER;

import java.io.IOException;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.NotificationDAO;
import model.Notification;

public class GetAllNotificationsServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = -5537268035896869667L;

    private static final Logger LOGGER = Logger.getLogger(GetAllNotificationsServlet.class);

    public GetAllNotificationsServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	Object currentlyLoggedInFlat = request.getSession().getAttribute("currentFlat");
	Object currentlyLoggedInFlatStatus = request.getSession().getAttribute("flatStatus");

	try {
	    if (currentlyLoggedInFlat == null || currentlyLoggedInFlatStatus == null) {
		response.sendRedirect("Login.jsp");
		return;
	    }

	    Vector<Notification> notifications = new NotificationDAO(ENTITY_MANAGER).showInfoAboutAllNotifications();
	    request.setAttribute("allNotifications", notifications);

	    request.getRequestDispatcher("ViewAllNotifications.jsp").forward(request, response);
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
    }

}
