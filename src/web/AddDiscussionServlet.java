package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.DiscussionDAO;
import model.Discussion;
import utils.common.EntityManagerService;
import utils.common.ValidationService;

public class AddDiscussionServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = -1818075838152960355L;

    private static final Logger LOGGER = Logger.getLogger(AddDiscussionServlet.class);

    public AddDiscussionServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    response.sendRedirect("GetAllDiscussionsServlet");
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	String title = String.valueOf(request.getParameter("title"));
	String content = String.valueOf(request.getParameter("content"));

	if (ValidationService.foundNullAttribute(title, content)) {
	    String errorReport = "No(null) value for a discussion related attribute found:" // nl
		    + "\n Title: " + title // nl
		    + "\n Content: " + content;
	    LOGGER.error(errorReport);
	    throw new ServletException(errorReport);
	}

	Discussion discussion = new Discussion();
	discussion.setTitle(title);
	discussion.setContent(content);

	new DiscussionDAO(EntityManagerService.ENTITY_MANAGER).addDiscussion(discussion);

	doGet(request, response);
    }

}
