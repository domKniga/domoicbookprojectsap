package web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import dao.AnswerDAO;
import exceptions.FilterRecordsForEntityException;
import exceptions.NoRecordForEntityException;
import exceptions.UnknownEntityException;
import model.Answer;
import model.Flat;
import utils.common.EntityManagerService;
import utils.common.ValidationService;

public class AddAnswerServlet extends HttpServlet {

    /**
     * Modified - object was altered significantly.
     */
    private static final long serialVersionUID = 7265419887677204696L;

    private static final Logger LOGGER = Logger.getLogger(AddAnswerServlet.class);

    public AddAnswerServlet() {
	super();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	try {
	    response.sendRedirect("GetAllDiscussionsServlet");
	} catch (IOException ie) {
	    LOGGER.error(ie.getMessage());
	    throw new ServletException(ie.getMessage(), ie);
	}
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException {
	Integer flatNumber = ((Flat) request.getSession().getAttribute("currentFlat")).getNumber();
	Integer discussionId = (Integer) request.getSession().getAttribute("chosen");
	String content = String.valueOf(request.getParameter("content"));

	if (ValidationService.foundNullAttribute(flatNumber, content, discussionId)) {
	    String errorReport = "No(null) value for an answer related attribute found:" // nl
		    + "\n Flat: " + flatNumber // nl
		    + "\n Discussion: " + discussionId // nl
		    + "\n Content: " + content;
	    LOGGER.error(errorReport);
	    throw new ServletException(errorReport);
	}

	Answer answer = new Answer();
	answer.setContent(content);

	try {
	    new AnswerDAO(EntityManagerService.ENTITY_MANAGER).addAnswer(answer, discussionId, flatNumber);
	} catch (UnknownEntityException | NoRecordForEntityException | FilterRecordsForEntityException e) {
	    LOGGER.error(e.getMessage());
	    throw new ServletException(e.getMessage(), e);
	}

	doGet(request, response);
    }

}
