package dao;

import java.util.Calendar;
import java.util.List;
import java.util.Vector;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import exceptions.FilterRecordsForEntityException;
import exceptions.NoRecordForEntityException;
import exceptions.UnknownEntityException;
import model.Answer;
import model.Discussion;
import model.Flat;
import utils.common.EntityManagerService;

/**
 * DAO(access) class to handle operations concerning the Answer model entity.
 * 
 * @authors Georgi Iliev, Vencislav Penev
 */
public class AnswerDAO {

    private static final Logger LOGGER = Logger.getLogger(AnswerDAO.class);

    private EntityManager entityManager;

    public AnswerDAO(EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    /**
     * Attempts to post an answer to a discussion and returns a boolean
     * representing the result.
     * 
     * @param newAnswer
     *            - the new answer(comment).
     * @param discussionId
     *            - the discussion identifier.
     * @param flatNumber
     *            - the flat(apartment) number posting the answer.
     * @return TRUE if answer was posted, FALSE otherwise.
     * @throws NoRecordForEntityException
     * @throws UnknownEntityException
     * @throws FilterRecordsForEntityException
     */
    public boolean addAnswer(Answer newAnswer, int discussionId, int flatNumber)
	    throws UnknownEntityException, NoRecordForEntityException, FilterRecordsForEntityException {
	try {
	    entityManager.getTransaction().begin();
	    LOGGER.info("Begin transaction: " + Calendar.getInstance().getTime());

	    Flat flat = (Flat) EntityManagerService.find(Flat.class, flatNumber);
	    Discussion discussion = (Discussion) EntityManagerService.find(Discussion.class, discussionId);

	    if (isAlreadyAnsweredByThisFlat(discussion, flat)) {
		return false;
	    }

	    newAnswer.setFlat(flat);
	    newAnswer.setDiscussion(discussion);
	    newAnswer.setDate();

	    entityManager.persist(newAnswer);
	    entityManager.getTransaction().commit();
	    LOGGER.info("Commit passed: " + Calendar.getInstance().getTime());

	    return true;
	} finally {
	    if (entityManager.getTransaction().isActive()) {
		entityManager.getTransaction().rollback();
		LOGGER.warn("Transaction failed for answer by flat (" + newAnswer.getFlat().getNumber()
			+ ") Performing rollback.");
	    }
	}
    }

    private boolean isAlreadyAnsweredByThisFlat(Discussion discussion, Flat flat)
	    throws FilterRecordsForEntityException {
	String answerFilterQuery = "SELECT u FROM Answer u WHERE u.discussion=:discussion AND u.flat=:flat";

	TypedQuery<Answer> answerFilter = entityManager.createQuery(answerFilterQuery, Answer.class);
	// EntityManagerProvider.createQuery(answerFilterQuery, Answer.class);
	answerFilter.setParameter("discussion", discussion);
	answerFilter.setParameter("flat", flat);

	return (findAnswerOrNull(answerFilter) != null);
    }

    private Answer findAnswerOrNull(TypedQuery<Answer> answerFilter) throws FilterRecordsForEntityException {
	List<Answer> results = null;

	try {
	    results = answerFilter.getResultList();
	} catch (RuntimeException re) {
	    LOGGER.warn("Problem with filtering: " + re.getMessage());
	    throw new FilterRecordsForEntityException(Answer.class.getSimpleName(), re);
	}

	return (results != null) ? results.get(0) : null;
    }

    /**
     * @param discussId
     * @return
     */
    public Vector<Answer> getInfoAboutDiscuss(int discussId) {
	Discussion discussion = entityManager.find(Discussion.class, discussId);
	String answerFilterQuerry = "SELECT u FROM Answer u WHERE u.discussion=:discussion";

	TypedQuery<Answer> answerFilter = entityManager.createQuery(answerFilterQuerry, Answer.class);
	answerFilter.setParameter("discussion", discussion);

	return (Vector<Answer>) answerFilter.getResultList();
    }
}