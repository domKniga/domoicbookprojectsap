<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="test.css"/>
	<title>New Notification</title>
</head>
<body>
	<center>
		<fieldset class="head">
			<p>
				<input type="Submit" class="btn btn-danger btn-lg" value="Home" onclick="window.location='index.jsp';" >
				<input type="Submit" class="btn btn-danger btn-lg" value="Discussions" onclick="window.location='GetAllDiscussionsServlet';">
				<input type="Submit" class="btn btn-danger btn-lg" value="Obligations" onclick="window.location='CheckObligationServlet';">
				<input type="Submit" class="btn btn-danger btn-lg" value="Messages" onclick="window.location='InboxServlet';">
				<input type="Submit" class="btn btn-danger btn-lg" value="Notifications" onclick="window.location='GetAllNotificationsServlet';">
				<%
					if ("ADMIN".equals(request.getSession().getAttribute("flatStatus"))) {
						out.print("<input type='Submit' class='btn btn-danger btn-lg' value='Flats' onclick='window.location=\"AddApartment.jsp\";'>");
					}
				%>
				<input type="Submit" class="btn btn-danger btn-lg"value="Logout" onclick="window.location='LogoutServlet';">
			</p>
		</fieldset>
	</center>
	
	<center>
		<fieldset class="book">
			<form action="AddNotificationServlet" method="post">
				<h1><b>New Notification</b></h1>
				
				<h3>
					<label class="field" for="title">Title:</label>
					<input type="text" name="title" id="title"/>
				</h3>
				
				<div class="form">
					<textarea name="content" rows="10" cols="30" ></textarea>
					<h4><input type="Submit" class="btn btn-danger btn-lg" value="Post Notification"></h4>
				</div>
			</form>
		</fieldset>
	</center>
</body>
</html>